# cpython-docker

Statically linked, relocatable CPython distribution for GNU/Linux.

# Motivation
Python (specifically CPython) is typically come as a part of the GNU/Linux distribution. For developers, this is very convenient since there is no separate steps for installing Python compared to Windows or macOS. However, depending on the distribution, you might be using an slightly older version of CPython. This is fine for most of the Python developers, but what if you want to use the latest version of CPython for new features(like pattern matching in 3.10) or just a different version for testing compatiblity, things can get a bit tricky.

Since Python offically does not distribute binaries for GNU/Linux, you are pretty much left with compling Python yourself. This sounds easy, but there are some issues there: Python has a relatively complex dependency situation thus you will need to install a lot of compile time and runtime dependencies. This will also take a long time if your machine is slow. To make the problem worse, sometimes the machine you target might not be your developement machine and you might not able to install dependencies to compile there.

This is exactly what ```cpython-docker``` is trying to solve, by making a statically linked, relocatable CPython distribution. You only need to download the binary, unarchive it and use it. No need to figure out dependencies and no need to compile it manually(You are of course welcomed to do so if you want).

# Usage

Since this project is still not very mature, binary download is not available yet. To use it now you need to have Docker installed. Then just execute ```build3.X.sh```(X is the Python major version). We currently support Python 3.9, Python 3.10, Python 3.11, Python 3.12 and Python 3.13.

Supported architectures:

- x86_64
- aarch64
- ppc64le
- s390x (need to specify `clefos:7` image in Dockerfile for now)

## Deploy Python

Uncompress the tar.xz file and change directory into it.

```bash
./bin/python3.X deploy.py
```
Note that X here is the Python major version and `deploy.py` can only be run in the same directory where it is located.

# License

The build scripts are licensed under GNU General Public License Version 3.
