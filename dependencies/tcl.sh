#!/usr/bin/env bash

set -ex

tcl_ver=8.6.16

cd /root
wget https://prdownloads.sourceforge.net/tcl/tcl$tcl_ver-src.tar.gz

tar -xvf tcl$tcl_ver-src.tar.gz
cd tcl$tcl_ver/

rm -rf pkgs/sqlite* pkgs/tdbc*

cd unix

CFLAGS="-fPIC -I/usr/include" CPPFLAGS="-fPIC -I/usr/include" ./configure --prefix=/usr --enable-shared=no --enable-threads
make -j$((`nproc`+1))
make install
