#!/usr/bin/env bash

set -euo pipefail

xz_ver=5.6.4

cd /root
wget https://tukaani.org/xz/xz-$xz_ver.tar.gz

tar -xvf xz-$xz_ver.tar.gz
cd xz-$xz_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" CCASFLAGS="-fPIC" ./configure --prefix=/usr --disable-shared
make -j$((`nproc`+1))
make install
