#!/usr/bin/env bash

set -ex

ncurses_ver=6.5

cd /root
wget https://invisible-mirror.net/archives/ncurses/ncurses-$ncurses_ver.tar.gz

tar -xvf ncurses-$ncurses_ver.tar.gz
cd ncurses-$ncurses_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --without-tests --without-manpages --disable-stripping --enable-widec
make -j$((`nproc`+1))
make install
