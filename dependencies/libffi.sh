#!/usr/bin/env bash

set -ex

libffi_ver=3.4.6

cd /root
wget https://github.com/libffi/libffi/releases/download/v$libffi_ver/libffi-$libffi_ver.tar.gz

tar -xvf libffi-$libffi_ver.tar.gz
cd libffi-$libffi_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --disable-shared
make -j$((`nproc`+1))
make install
