#!/usr/bin/env bash

set -ex

tk_ver=8.6.16

cd /root
wget https://prdownloads.sourceforge.net/tcl/tk$tk_ver-src.tar.gz

tar -xvf tk$tk_ver-src.tar.gz
cd tk$tk_ver/unix/

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --enable-threads --enable-shared=no --with-tcl=/usr/lib --x-includes=/usr/include/X11 --x-libraries=/usr/lib/X11
make -j$((`nproc`+1))
make install
