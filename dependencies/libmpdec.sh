#!/usr/bin/env bash

set -ex

mpdecimal_ver=4.0.0

cd /root
wget https://www.bytereef.org/software/mpdecimal/releases/mpdecimal-$mpdecimal_ver.tar.gz

tar -xvf mpdecimal-$mpdecimal_ver.tar.gz
cd mpdecimal-$mpdecimal_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --enable-static=yes --enable-shared=no
make -j$((`nproc`+1))
make install
