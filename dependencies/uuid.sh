#!/usr/bin/env bash

set -euo pipefail

util_linux_major_ver=2.40
util_linux_full_ver=2.40.4

cd /root
wget https://mirrors.edge.kernel.org/pub/linux/utils/util-linux/v$util_linux_major_ver/util-linux-$util_linux_full_ver.tar.gz

tar -xvf util-linux-$util_linux_full_ver.tar.gz
cd util-linux-$util_linux_full_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --disable-all-programs --enable-libuuid
make -j$((`nproc`+1))
make install
