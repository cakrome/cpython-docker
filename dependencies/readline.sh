#!/usr/bin/env bash

set -ex

readline_ver=8.2

cd /root
wget https://ftp.gnu.org/gnu/readline/readline-$readline_ver.tar.gz

tar -xvf readline-$readline_ver.tar.gz
cd readline-$readline_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" LDFLAGS="-L/usr/lib" ./configure --prefix=/usr --disable-shared --with-curses
make -j$((`nproc`+1))
make install
chmod +x /lib/libreadline.a
