#!/usr/bin/env bash

set -ex

gdbm_ver=1.24

cd /root
wget https://ftp.gnu.org/gnu/gdbm/gdbm-$gdbm_ver.tar.gz

tar -xvf gdbm-$gdbm_ver.tar.gz
cd gdbm-$gdbm_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --disable-shared --enable-libgdbm-compat
make -j$((`nproc`+1))
make install
