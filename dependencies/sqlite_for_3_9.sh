#!/usr/bin/env bash

set -euo pipefail

sqlite_ver=3480000

cd /root
wget https://www.sqlite.org/2025/sqlite-autoconf-$sqlite_ver.tar.gz

tar -xvf sqlite-autoconf-$sqlite_ver.tar.gz
cd sqlite-autoconf-$sqlite_ver

CFLAGS="-fPIC" CPPFLAGS="-fPIC" ./configure --prefix=/usr --disable-shared
make -j$((`nproc`+1))
make install
