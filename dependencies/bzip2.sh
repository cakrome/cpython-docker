#!/usr/bin/env bash

set -ex

cd /root
wget https://sourceware.org/pub/bzip2/bzip2-1.0.8.tar.gz

tar -xvf bzip2-1.0.8.tar.gz
cd bzip2-1.0.8

make -j$((`nproc`+1)) install CFLAGS="-fPIC" PREFIX=/usr
