#!/usr/bin/env bash

set -euo pipefail

py_major_ver=3
py_minor_ver=10
py_micro_ver=16
py_ver=$py_major_ver.$py_minor_ver.$py_micro_ver

sudo docker build "$@" -f Dockerfile.3.10 . -t cakrome/cpython310 --build-arg PYTHON_MAJOR_VERSION=$py_major_ver --build-arg PYTHON_MINOR_VERSION=$py_minor_ver --build-arg PYTHON_MICRO_VERSION=$py_micro_ver
id=$(sudo docker create cakrome/cpython310)
sudo docker cp "$id":/root/build/python$py_ver.tar.xz .
sudo docker rm -v "$id"
sudo chown "$USER":"$USER" python$py_ver.tar.xz
#patchelf --add-rpath \$ORIGIN/../lib/ python3.10
