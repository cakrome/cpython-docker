#!/usr/bin/env bash

set -euo pipefail

sys_arch="$(uname -m)"

if [ "${sys_arch}" != "s390x" ]
then
	sed -i s/mirror.centos.org/vault.centos.org/g /etc/yum.repos.d/*.repo
	sed -i s/^#.*baseurl=http/baseurl=http/g /etc/yum.repos.d/*.repo
	sed -i s/^mirrorlist=http/#mirrorlist=http/g /etc/yum.repos.d/*.repo

	if [ "${sys_arch}" == "aarch64" ] || [ "${sys_arch}" == "ppc64le" ]
	then
		sed -i 's;/centos/7/;/altarch/7/;g' /etc/yum.repos.d/*.repo
	fi
fi
