import glob
import os
from pathlib import Path


def get_python_minor_version() -> str:
    minor_ver: str = glob.glob("bin/pydoc3*")[0].split(".")[1]

    return minor_ver


def python_executable_fix(current_directory: str, py_minor_ver: str) -> None:

    main_py_exe: str = f"bin/python3.{py_minor_ver}"

    list_file_to_modify: list[str] = [
        f"bin/idle3.{py_minor_ver}",
        f"bin/pydoc3.{py_minor_ver}",
        f"bin/pip3.{py_minor_ver}",
    ]

    if int(py_minor_ver) < 13:
        list_file_to_modify.append(f"bin/2to3-3.{py_minor_ver}")

    config_exe: str = glob.glob(
        f"lib/python3.{py_minor_ver}/config-3.{py_minor_ver}-*-linux-gnu/python-config.py"
    )[0]
    list_file_to_modify.append(config_exe)

    for file in list_file_to_modify:
        file_lines: list[str] = []
        with open(file) as fr:
            file_lines = fr.readlines()
        file_lines[0] = f"#!{current_directory}" + f"/{main_py_exe}\n"
        with open(file, "w") as fw:
            fw.writelines(file_lines)


def python_config_exe_fix(current_directory: str, py_minor_ver: str) -> None:

    config_exe: str = f"bin/python3.{py_minor_ver}-config"
    pref: str = "prefix="

    file_lines: list[str] = []

    with open(config_exe) as f:
        file_lines = f.readlines()

    ele = ""
    for i in file_lines:
        if i.startswith(pref):
            ele = i
    ind = file_lines.index(ele)
    file_lines[ind] = pref + f'"{current_directory}"\n'

    with open(config_exe, "w") as config_exe_done:
        config_exe_done.writelines(file_lines)


def python_sysconfigdata_fix(current_directory: str, py_minor_ver: str) -> None:

    current_path: Path = Path(current_directory)
    parent_path: Path = current_path.parent.absolute()
    sysconfigdata_file_path: str = glob.glob(
        str(current_path)
        + f"/lib/python3.{py_minor_ver}/_sysconfigdata__linux_*-linux-gnu.py"
    )[0]

    real_sysconfigdata_file: str = ""
    with open(sysconfigdata_file_path) as sysconfigdata_file:
        real_sysconfigdata_file = sysconfigdata_file.read()

    fixed_contents: str = real_sysconfigdata_file.replace(
        "/root/build", str(parent_path)
    ).replace("/root", str(parent_path))

    with open(sysconfigdata_file_path, "w") as f:
        f.write(fixed_contents)


def main() -> None:

    py_minor_ver: str = get_python_minor_version()
    current_path: str = os.getcwd()

    python_executable_fix(current_path, py_minor_ver)
    python_config_exe_fix(current_path, py_minor_ver)
    python_sysconfigdata_fix(current_path, py_minor_ver)


if __name__ == "__main__":
    main()
